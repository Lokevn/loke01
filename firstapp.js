var express = require('express');
var app = express();

//setup
var server_port = process.env.OPENSHIFT_NODEJS_PORT || 8080;
var server_ip_address = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';

app.get('/', function(req, res) {
	res.end('Hi Guys!');
});

// server listen on common port 8080 8
app.listen(server_port, server_ip_address, function() {
	console.log("listening on " + server_ip_address + server_port);
});
